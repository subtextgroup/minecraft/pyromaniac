**Pyromaniac**


Permission: pm.pyromaniac

Example: /pex user GyroBrine add pm.pyromaniac

Usage: /pyromaniac on|off

Invoking plugin without the pm.pyromaniac permission has unpleasant consequences for the player.
