package com.sg.mc.pm;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Ryan on 4/9/2017.
 */
public class Pyromaniac extends JavaPlugin implements Listener, CommandExecutor {

    public static final String PYROMANIAC_ACTIVE_METADATA_KEY = "pyromaniac.active";
    public static final String PYROMANIAC_WANNABE_METADATA_KEY = "pyromaniac.wannabe";
    public static final long DELAY = 20 * 30;

    @Override
    public void onEnable() {

        getCommand("pyromaniac").setExecutor(this);

        getServer().getPluginManager().registerEvents(this, this);
        super.onEnable();
    }



    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if(p.hasMetadata(PYROMANIAC_ACTIVE_METADATA_KEY)) {
            if(!e.getFrom().getBlock().equals(e.getTo().getBlock())) {
                e.getFrom().subtract(0, 1, 0).getBlock().getRelative(BlockFace.UP).setType(Material.FIRE);
            }
        } else if(p.hasMetadata(PYROMANIAC_WANNABE_METADATA_KEY)) {
            if(!e.getFrom().getBlock().equals(e.getTo().getBlock())) {
                final Spider s = (Spider)p.getWorld().spawnEntity(e.getFrom(), EntityType.SPIDER);
                s.setTarget(p);
                s.setGlowing(true);
                final JavaPlugin plugin = this;
                getServer().getScheduler().runTaskLater(plugin, () -> s.remove(), DELAY);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onBurn(EntityDamageEvent e) {
        if(Player.class.isInstance(e.getEntity()) && ((Player)e.getEntity()).hasMetadata(PYROMANIAC_ACTIVE_METADATA_KEY)) {
            if(e.getCause().equals(EntityDamageEvent.DamageCause.FIRE) || e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerKilled(PlayerDeathEvent e) {
        if(Player.class.isInstance(e.getEntity())) {
            Player p = (Player)e.getEntity();

            if(p.hasMetadata(PYROMANIAC_WANNABE_METADATA_KEY)) {
                removeSpiders(p);
                p.sendMessage("§aMaybe next time you shouldn't meddle with unknown commands ;)");
            } else if(p.hasMetadata(PYROMANIAC_ACTIVE_METADATA_KEY)) {
                removePyro(p);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerQuit(PlayerQuitEvent e) {
        removePyro(e.getPlayer());
    }
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerKicked(PlayerKickEvent e) {
        removePyro(e.getPlayer());
    }

    private void removePyro(Player p) {
        p.removeMetadata(PYROMANIAC_ACTIVE_METADATA_KEY, this);
    }

    private void removeSpiders(Player p) {
        p.removeMetadata(PYROMANIAC_WANNABE_METADATA_KEY, this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(!commandSender.isPermissionSet("pm.pyromaniac")) {
            commandSender.sendMessage("You do not have the pm.pyromaniac permission.");
            if(Player.class.isInstance(commandSender) && args.length > 0) {
                spiders((Player)commandSender);
                return true;
            }
            return false;
        }

        if(!Player.class.isInstance(commandSender) && args.length != 2) {
            commandSender.sendMessage("Only players can issue this command");
            return false;
        }

        if(args.length == 0 || args.length > 2) {
            return false;
        }


        Player p = null;

        if(args.length == 2) {
            p = getServer().getPlayer(args[1]);
            if(p == null) {
                commandSender.sendMessage("No player named " + args[1] + " was found");
                return true;
            }
        } else {
            p = (Player) commandSender;
        }


        if("on".equalsIgnoreCase(args[0])) {
            p.setMetadata(PYROMANIAC_ACTIVE_METADATA_KEY, new FixedMetadataValue(this, "true"));
            p.chat("§cFIRE FIRE FIRE FIRE!");
            return true;
        } else if("off".equalsIgnoreCase(args[0])) {
            removePyro(p);
        } else {
            return false;
        }
        return true;
    }
    private void spiders(final Player p) {
        p.sendMessage("§cENJOY " + DELAY / 20 + " SECONDS OF FIRE, NAUSEA, AND ANGRY SPIDERS! RUN!");
        p.chat("§cI DID A BAD THING! AAAAARRRGGGHHHHH! EVERYBODY LAUGH AT ME!");
        final JavaPlugin plugin = this;

        getServer().getScheduler().runTaskLater(plugin, () -> removeSpiders(p), DELAY);
        p.setMetadata(PYROMANIAC_WANNABE_METADATA_KEY, new FixedMetadataValue(this, "true"));
        p.setHealth(Math.min(p.getHealth() * 3.0, p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()));
        p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, (int)DELAY, 5));
        p.setFireTicks(p.getFireTicks() + (int)DELAY);
        getServer().getScheduler().runTaskLater(plugin, () -> p.teleport(p.getLocation().add(1,0,0)), 5L);

    }

}
